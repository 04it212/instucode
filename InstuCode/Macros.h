//
//  Macros.h
//  InstaCode
//
//  Created by SM on 12/22/14.
//  Copyright (c) 2014 codeapper. All rights reserved.
//

#ifndef InstaCode_Macros_h
#define InstaCode_Macros_h

#define UI_IMAGE_FROM_NAME(v)       [UIImage imageNamed:v]

#define UI_COLOR_FROM_HEX(Hex) [UIColor colorWithRed:((float)((Hex & 0xFF0000) >> 16))/255.0 green:((float)((Hex & 0xFF00) >> 8))/255.0 blue:((float)(Hex & 0xFF))/255.0 alpha:1.0]

#define UI_COLOR_FROM_RGB(r, g, b)  [UIColor colorWithRed:((r) / 255.0) green:((g) / 255.0) blue:((b) / 255.0) alpha:1.0]

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

#define URL(v)                                      [NSURL URLWithString:v]

#define MAlert(TITLE,MSG) [[[UIAlertView alloc] initWithTitle:(TITLE) \
message:(MSG) \
delegate:nil \
cancelButtonTitle:@"OK" \
otherButtonTitles:nil] show]

#define STRINGS(...) [@[__VA_ARGS__] componentsJoinedByString:@""]


#endif

