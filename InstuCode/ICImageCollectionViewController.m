//
//  ICImageCollectionViewController.m
//  InstuCode
//
//  Created by SM on 1/11/15.
//  Copyright (c) 2015 codeapper. All rights reserved.
//

#import "ICImageCollectionViewController.h"
#import "InstagramEngine.h"
#import "InstagramMedia.h"
#import "InstagramPaginationInfo.h"
#import "ICImageCollectionViewCell.h"
#import "UIImageView+AFNetworking.h"
#import "InstagramMedia+Additionals.h"
#import "NSMutableArray+InstagramMediaSorting.h"

@interface ICImageCollectionViewController ()

@property (nonatomic, strong, readwrite) NSMutableArray *imagesArray;
@property (nonatomic, strong) InstagramPaginationInfo   *paginationInfo;
@property (nonatomic, assign) CGFloat                   lastOffset;
@property (nonatomic, assign) ScrollDirection           collectionViewScrollDirection;

@end

@implementation ICImageCollectionViewController

static NSString *const reuseIdentifier = @"Cell";
static NSString *const hashTagToSearch = @"selfie";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    
    self.title = STRINGS(@"#",hashTagToSearch);
    self.imagesArray = [NSMutableArray array];
    [self fetchImagesForHashTag:hashTagToSearch];
}

- (void)fetchImagesForHashTag:(NSString *)hashTag
{
    [[InstagramEngine sharedEngine] getMediaWithTagName:hashTag count:20 maxId:self.paginationInfo.nextMaxId withSuccess:^(NSArray *media, InstagramPaginationInfo *paginationInfo) {
        
        [self.imagesArray addObjectsFromArray:[[NSMutableArray arrayWithArray:media] sortAccordingToStandardImageArea]];
        NSLog(@"Count %lu ",(unsigned long)[self.imagesArray count]);
        
        self.paginationInfo = paginationInfo;
        [self.collectionView reloadData];
        
    } failure:^(NSError *error) {
        NSLog(@"Search Media Failed");
    }];
}

/**
    Images will be sorted according to standardResolutionImageSize
    Algorithm - quicksort
 */
- (IBAction)sortImages:(id)sender
{
    //TODO : Change Y-Offset to something meaningful by logic
    [self.collectionView setContentOffset:CGPointMake(0.0, - 65.0) animated:YES];
    self.imagesArray = [self.imagesArray sortBasedOn:0 andLast:self.imagesArray.count - 1];
    [self.collectionView reloadData];
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.imagesArray count];
}

- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    return 1;
}

- (ICImageCollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ICImageCollectionViewCell *cell = (ICImageCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    cell.layer.shouldRasterize = YES;
    cell.layer.rasterizationScale = [UIScreen mainScreen].scale;
    cell.backgroundColor = [UIColor blackColor];
    
    InstagramMedia *media = [self.imagesArray objectAtIndex:indexPath.row];
    
    NSLog(@"StandardImageSize %f ",[media area]);
    [cell.thumbnailImageView setImageWithURL:media.thumbnailURL placeholderImage:UI_IMAGE_FROM_NAME(@"transparent.png")];
    
    return cell;
}

#pragma UIScrollViewDelegate

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    self.lastOffset = scrollView.contentOffset.y;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (self.lastOffset > scrollView.contentOffset.y)
    {
        self.collectionViewScrollDirection = ScrollDirectionUp;
    }
    else if (self.lastOffset < scrollView.contentOffset.y)
    {
        self.collectionViewScrollDirection = ScrollDirectionDown;
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (self.collectionViewScrollDirection == ScrollDirectionDown)
    {
        [[InstagramEngine sharedEngine] getMediaWithTagName:hashTagToSearch count:20 maxId:self.paginationInfo.nextMaxId withSuccess:^(NSArray *media, InstagramPaginationInfo *paginationInfo) {
            
            NSInteger startIndex = self.imagesArray.count;
            
            [self.imagesArray addObjectsFromArray:[NSMutableArray arrayWithArray:media]];
            self.paginationInfo = paginationInfo;
            
            NSMutableArray *newIndexPaths = [NSMutableArray array];
            
            for (NSInteger i = startIndex; i < self.imagesArray.count; i++) {
                [newIndexPaths addObject:[NSIndexPath indexPathForRow:i inSection:0]];
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.collectionView insertItemsAtIndexPaths:newIndexPaths];
                
                [self.collectionView scrollToItemAtIndexPath:[newIndexPaths objectAtIndex:0] atScrollPosition:UICollectionViewScrollPositionCenteredVertically animated:YES];
            });
        } failure:^(NSError *error) {
            NSLog(@"Search Media Failed");
        }];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end