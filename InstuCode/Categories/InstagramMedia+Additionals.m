//
//  InstagramMedia+Additionals.m
//  InstuCode
//
//  Created by SM on 1/11/15.
//  Copyright (c) 2015 codeapper. All rights reserved.
//

#import "InstagramMedia+Additionals.h"

@implementation InstagramMedia (Additionals)

- (float)area
{
    return self.standardResolutionImageFrameSize.height*self.standardResolutionImageFrameSize.width;
}

@end
