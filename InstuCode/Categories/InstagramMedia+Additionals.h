//
//  InstagramMedia+Additionals.h
//  InstuCode
//
//  Created by SM on 1/11/15.
//  Copyright (c) 2015 codeapper. All rights reserved.
//

#import "InstagramMedia.h"

@interface InstagramMedia (Additionals)

- (float)area;

@end
