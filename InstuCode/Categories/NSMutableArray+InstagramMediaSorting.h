//
//  NSMutableArray+InstagramMediaSorting.h
//  InstuCode
//
//  Created by SM on 1/11/15.
//  Copyright (c) 2015 codeapper. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableArray (InstagramMediaSorting)

- (NSMutableArray *)sortAccordingToStandardImageArea;
- (NSMutableArray *)sortBasedOn:(int)first andLast:(int)last;

@end
