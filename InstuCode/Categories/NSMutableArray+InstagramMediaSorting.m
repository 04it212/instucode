//
//  NSMutableArray+InstagramMediaSorting.m
//  InstuCode
//
//  Created by SM on 1/11/15.
//  Copyright (c) 2015 codeapper. All rights reserved.
//

#import "NSMutableArray+InstagramMediaSorting.h"
#import "InstagramMedia.h"
#import "InstagramMedia+Additionals.h"

@implementation NSMutableArray (InstagramMediaSorting)

// This sort is used for image sets of 20, infinite scroll fetches 20 at one
- (NSMutableArray *)sortAccordingToStandardImageArea
{
    NSMutableArray *smallElementsArray = [[NSMutableArray alloc] init];
    NSMutableArray *biggerElementsArray =[[NSMutableArray alloc] init];
    
    if ([self count] < 1)
    {
        return self;
    }
    
    int randomPivotPoint = arc4random() % [self count];
    InstagramMedia *pivotMediaObject = [self objectAtIndex:randomPivotPoint];
    [self removeObjectAtIndex:randomPivotPoint];
    
    for (InstagramMedia *mediaObject in self)
    {
        long areaOfPivot = pivotMediaObject.area;
        long areaOfMediaObject = mediaObject.area;
        
        if (areaOfMediaObject > areaOfPivot)
        {
            [biggerElementsArray addObject:mediaObject];
        }
        else
        {
            [smallElementsArray addObject:mediaObject];
        }
    }
    NSMutableArray *sortedArray = [[NSMutableArray alloc] init];
    [sortedArray addObjectsFromArray:[biggerElementsArray sortAccordingToStandardImageArea]];
    [sortedArray addObject:pivotMediaObject];
    [sortedArray addObjectsFromArray:[smallElementsArray sortAccordingToStandardImageArea]];
    
    return sortedArray;
}

// This is advantageous when memory is concern
// while sorting all images , this to be used
- (NSMutableArray *)sortBasedOn:(int)first andLast:(int)last
{
    int pivot,j,i;
    
    InstagramMedia *temporaryMediaObject = nil;
    
    if (first < last) {
        pivot = first;
        i = first;
        j = last;
        
        while (i < j) {
            InstagramMedia *mediaObjectToCompare = nil;
            InstagramMedia *pivotMediaObject = [self objectAtIndex:pivot];
            
            while ((mediaObjectToCompare = [self objectAtIndex:i]) && [mediaObjectToCompare area] >= [pivotMediaObject area] && i < last) {
                i++;
            }
            
            while ((mediaObjectToCompare = [self objectAtIndex:j]) && [mediaObjectToCompare area] < [pivotMediaObject area]) {
                j--;
            }
            
            if (i < j) {
                temporaryMediaObject = [self objectAtIndex:i];
                [self replaceObjectAtIndex:i withObject:[self objectAtIndex:j]];
                [self replaceObjectAtIndex:j withObject:temporaryMediaObject];
            }
        }
        
        temporaryMediaObject = [self objectAtIndex:pivot];
        [self replaceObjectAtIndex:pivot withObject:[self objectAtIndex:j]];
        [self replaceObjectAtIndex:j withObject:temporaryMediaObject];
        
        [self sortBasedOn:first andLast:j-1];
        [self sortBasedOn:j+1 andLast:last];
    }
    
    return self;
}


@end