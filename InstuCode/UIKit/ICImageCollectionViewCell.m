//
//  ICImageCollectionViewCell.m
//  InstuCode
//
//  Created by SM on 1/11/15.
//  Copyright (c) 2015 codeapper. All rights reserved.
//

#import "ICImageCollectionViewCell.h"

@implementation ICImageCollectionViewCell

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    
    [self.layer setShadowOffset:CGSizeMake(1, 2)];
    [self.layer setShadowRadius:2.0];
    [self.layer setShadowColor:[UIColor grayColor].CGColor] ;
    [self.layer setShadowOpacity:1.0];
    [self.layer setMasksToBounds:NO];
    
    UIView *customColorView = [[UIView alloc] init];
    customColorView.backgroundColor = [UIColor lightGrayColor];
    self.selectedBackgroundView =  customColorView;
}

@end
