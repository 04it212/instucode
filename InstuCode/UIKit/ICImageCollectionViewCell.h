//
//  ICImageCollectionViewCell.h
//  InstuCode
//
//  Created by SM on 1/11/15.
//  Copyright (c) 2015 codeapper. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ICImageCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) IBOutlet UIImageView *thumbnailImageView;

@end
