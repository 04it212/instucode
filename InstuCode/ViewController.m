//
//  ViewController.m
//  InstuCode
//
//  Created by SM on 1/11/15.
//  Copyright (c) 2015 codeapper. All rights reserved.
//

#import "ViewController.h"
#import "ICImageCollectionViewController.h"
#import "InstagramEngine.h"

@interface ViewController () <UIWebViewDelegate>

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    NSDictionary *configuration = [InstagramEngine sharedEngineConfiguration];
    
    
    NSURL *url = [NSURL URLWithString:STRINGS(configuration[kInstagramKitAuthorizationUrlConfigurationKey],@"?client_id=",configuration[kInstagramKitAppClientIdConfigurationKey],@"&redirect_uri=",configuration[kInstagramKitAppRedirectUrlConfigurationKey],@"&response_type=token")];
    
    self.instaAuthWebView.contentMode = UIViewContentModeScaleAspectFit;
    [self.instaAuthWebView loadRequest:[NSURLRequest requestWithURL:url]];
}


- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSString *URLString = [request.URL absoluteString];
    if ([URLString hasPrefix:[[InstagramEngine sharedEngine] appRedirectURL]]) {
        NSString *delimiter = @"access_token=";
        NSArray *components = [URLString componentsSeparatedByString:delimiter];
        if (components.count > 1) {
            NSString *accessToken = [components lastObject];
            NSLog(@"ACCESS TOKEN = %@",accessToken);
            [[InstagramEngine sharedEngine] setAccessToken:accessToken];
            
            self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Next" style:UIBarButtonItemStylePlain target:self action:@selector(showMediaViewController)];
            [self showMediaViewController];
        }
        return NO;
    }
    return YES;
}

- (void)showMediaViewController
{
    [self performSegueWithIdentifier:@"ImageCollectionViewController" sender:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
