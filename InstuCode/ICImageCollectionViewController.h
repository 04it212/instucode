//
//  ICImageCollectionViewController.h
//  InstuCode
//
//  Created by SM on 1/11/15.
//  Copyright (c) 2015 codeapper. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum ScrollDirection {
    ScrollDirectionNone,
    ScrollDirectionUp,
    ScrollDirectionDown,
} ScrollDirection;

@interface ICImageCollectionViewController : UICollectionViewController

@property (nonatomic, strong, readonly) NSMutableArray *imagesArray;

@end